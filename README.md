# RxBenefits Front End Project
## Development Quick Start

* Obtain an API key via [themoviedb.org](https://themoviedb.org/documentation/api)

Clone the repo

```bash
git clone git clone git@bitbucket.org:wad3g/rxb-project.git
$ cd rxb-project
```

Install project dependencies

```bash
# Install project dependencies
$ yarn
```

Run the app locally with `yarn start`

```bash
# Development server
$ yarn dev # starts dev server

```bash
# Build app
$ yarn build # Outputs to ./build directory
$ npx serve build # Static server for the built website
```

## Technologies
[x] React via `create-react-app`
[x] `react-router-dom` for use of `useParams` & `useHistory`
[x] Styled Components
[x] Styled System
[x] `dayjs` rather than `moment` it's size -- 2kb

### Others Included
[x] `.vscode` project settings
[x] `.editorconfig`
[x] `.prettierrc`


## Screenshots


### Instructions
# Ultimate Moviegoers Guide

## Overview

Your client has tasked you with making the Ultimate Moviegoers Guide using [_The Movie Database_ API](https://www.themoviedb.org/documentation/api). They are asking for an app that at least covers the following:

* Shows the user a list of movies where the user can filter by
  * Now Playing
  * Popular
  * Top Rated
* Allows the user to search for a movie
* Allows the user to select a movie and see more details - the more details, the better

Best of all is the client has given you full creative control.
Make the Ultimate Moviegoers Guide the best looking app out there!
Use React to build the application.

### Keep in mind the following questions

* What design patterns did you use?
* How would you test your application?
* How do you manage/store application state?
* What ways could you structure the code to make it easy to understand and maintain?
* What other considerations and tradeoffs did you make when building the application?

## API

1. [Sign Up](https://www.themoviedb.org/signup) for an account with [_The Movie Database_](https://www.themoviedb.org/)
1. Login and go to _Settings_ -> [_API_](https://www.themoviedb.org/settings/api)
1. Create a new developer API key:
   - Set the _Type of Use_ to "Personal"
   - If you do not have a website url, we recommend [creating a free github page](https://pages.github.com/).

## Helpful Links

[The Movie Database API Documentation](https://developers.themoviedb.org/3/getting-started/introduction)

## Submitting Your App
When you have completed your app, please post it in a public repository and send us a link - GitHub, GitLab, BitBucket, etc.

One last thing - as with all Agile development, we understand that everything might not get completed in this first sprint.  Focus on creating something you're proud of, and know that you won't be graded on completeness!
