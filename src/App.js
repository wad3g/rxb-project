import './App.css';

import { AppWrapper, Page } from './components';
import { BrowserRouter, Redirect, Route, Switch, useLocation } from 'react-router-dom';

import { AppRoute } from './routes';
import Layout from './components/Layouts';
import React from 'react';
import { config } from './utils/config';

const App = () => {

  return (
    <BrowserRouter>
      <AppWrapper>
        <div className='App'>
          <Switch>
            <Route exact path='/popular' render={() => <Redirect to='/' />} />
            {config.map((c, i) => (
              <AppRoute
                exact
                key={c.name.toLowerCase()}
                path={c.route}
                layout={Layout.Nav}
                children={(props) => (
                  <Page url={c.endpoint} title={c.name} {...props} />
                )}
              />
            ))}
          </Switch>
        </div>
      </AppWrapper>
    </BrowserRouter>
  );
};

export default App;
