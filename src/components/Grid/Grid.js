import React from 'react';
import Box from '../Box';

const Grid = props => <Box {...props} verticalAlign="top" px={2} />;

const Half = props => <Grid {...props} width={[1, 1 / 2]} />;

const Third = props => <Grid {...props} width={[1, 1 / 3]} />;

const Quarter = props => (
  <Grid {...props} display={['none', 'inline-flex']} width={[1, 1 / 4]} />
);

const TwoThird = props => <Grid {...props} width={[1, 2 / 3]} />;

const ThreeQuarter = props => <Grid {...props} width={[1, 3 / 4]} />;

const Flex = props => <Box {...props} display="flex" />;

const FlexAuto = props => <Box {...props} flex="1 1 auto" />;

export { Grid, Half, Third, Quarter, TwoThird, ThreeQuarter, Flex, FlexAuto}
