import React from 'react';
import PropTypes from 'prop-types';
import { HeroStyle } from './HeroStyle';


const Hero = ({image, title, text}) =>{
   return(
       <HeroStyle bg={image}>
           <div className = "heroimage-content">
               <div className = "heroimage-text">
                   <h1>{title}</h1>
                   <p>{text}</p>
               </div>
           </div>
       </HeroStyle>
   )
}
Hero.propTypes ={
    image: PropTypes.string,
}
export default Hero;
