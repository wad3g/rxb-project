import styled from 'styled-components';

export const HeroStyle = styled.div`

  background-image: linear-gradient(
    to bottom,
    rgba(23, 29, 54, 0.4),
    rgba(20, 22, 23, 1)
  ),
  ${(props) => `url(${props.bg})`};
  background-size: 100%, cover !important;
  width: 100%;
  height: 720px;
  position: absolute;
  top: 0;
  left: 0;
  animation: animateHeroimage 1s;
  z-index: -1;

  @keyframes animateHeroimage {
    from {
      opacity: 0;
    }
    to {
      opacity: 1;
    }
  }
`;
