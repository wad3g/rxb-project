import Box from '../Box';
import Menu from '../Menu';
import React from 'react';

const styles = {
  layout: {
    flex: 1,
    display: 'flex',
    alignItems: 'center',
    flexDirection: 'column',
  },
};
const Main = (props) => {
  return <Box style={styles.layout}>{props.children}</Box>;
};

Main.displayName = 'Main';

const Nav = (props) => {
  return (
    <>
      <Menu />
      <div className='main'>{props.children}</div>
    </>
  );
};

Nav.displayName = 'Nav';

const Layouts = {
  Main,
  Nav,
};

export default Layouts;
