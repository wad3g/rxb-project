import { MenuItemStyled, MenuStyled } from './MenuStyled';

import { NavLink } from 'react-router-dom';
import React from 'react';
import { config } from '../../utils/config';

const SearchIcon = () => (
  <>
    <svg
      className='svg-icon search-icon'
      xmlns='http://www.w3.org/2000/svg'
      height='18'
      viewBox='0 0 18 18'
      width='18'>
      <g className='search-path' fill='none' stroke='#9ba1a6' strokeWidth='2'>
        <path strokeLinecap='round' d='M18.5 18.3l-5.4-5.4' />
        <circle cx='8' cy='8' r='7' />
      </g>
    </svg>
  </>
);

const Menu = (props) => {
  return (
    <nav
      style={{
        display: 'flex',
        flexFlow: 'row',
        justifyContent: 'space-between',
      }}>
      <MenuStyled>
        <NavLink exact activeClassName='active' to='/'>
          <MenuItemStyled>
            <strong
              style={{
                fontSize: '1.4rem',
                color: 'var(--bg-color)',
                backgroundColor: 'var(--link-hover-color)',
                padding: '0.5rem',
                fontFamily: 'var(--fontFamilyMono',
              }}>
              UMG
            </strong>
          </MenuItemStyled>
        </NavLink>
      </MenuStyled>
      <MenuStyled>
        {config.map((c, i) => (
          <NavLink key={i} exact activeClassName='active' to={c.route}>
            <MenuItemStyled>
              <strong>{c.name}</strong>
            </MenuItemStyled>
          </NavLink>
        ))}
      </MenuStyled>
    </nav>
  );
};

export default Menu;
