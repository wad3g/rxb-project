import styled from 'styled-components';

export const MenuStyled = styled.ul`
  list-style-type: none;
  margin: 0;
  padding: 0;
  display: flex;
  justify-content: flex-end;
  align-items: center;
  padding-bottom: 0.5rem;

`;

export const MenuItemStyled = styled.li`
  cursor: pointer;
  display: flex;
  align-items: center;
  margin-right: 1rem;

  a:link {
  text-decoration: none;
  color: #9ba1a6;
}
a {
  display: flex;
  align-items: center;
}

.active {
  color: white;
}
`;



