import { PageWrapper, ThumbnailWrapper } from '../Wrappers';
import { use, useRouteMatch } from 'react-router-dom';

import Layout from '../Layouts';
import Loader from '../Loader';
import { POPULAR_URL } from '../../utils/constants';
import React from 'react';
import Thumbnail from '../Thumbnail';
import Title from '../Title';
import { tmdbConfig } from '../../utils/config';
import { useFetch } from '../../hooks';

var useConfig = tmdbConfig;

function Page(props, url, title, displayName) {
  const { data, isLoading, hasError } = useFetch(props.url);
  let movies = [];

  if (data && !isLoading) {
    movies = data.results;
  }

  if (isLoading) {
    <Loader />;
  }

  return (
    <Layout.Nav>
      <PageWrapper>
        <Title fontSize={[28, 48, 60]}>{props.title}</Title>

        <ThumbnailWrapper>
          {movies &&
            movies.map((movie, idx) => (
              <Thumbnail
                key={idx}
                className='card'
                clickable
                image={
                  movie.backdrop_path
                    ? useConfig.imageUrl +
                      useConfig.backdrops.hero +
                      movie.backdrop_path
                    : ''
                }
                movieId={movie.id}
                movieName={movie.original_title}
                voteCount={movie.vote_count}
                title={movie.title}
              />
            ))}
        </ThumbnailWrapper>

        {hasError && <span>There was an issue fetching your data.</span>}
      </PageWrapper>
    </Layout.Nav>
  );
}

export default Page;
