import React, { useState } from 'react';
import { SearchBarContentStyle } from './SearchStyle';

const SearchBar = ({ searchMovieByTerm }) => {
  const [inputVal, setInputVal] = useState('');

  const formSubmit = (e) => {
    e.preventDefault();
    searchMovieByTerm(inputVal);
  };

  const handleChange = (e) => {
    e.preventDefault();
    setInputVal(e.target.value);
  };
  return (
    <SearchBarContentStyle>
      <form id='form' onSubmit={formSubmit}>
        <input
          type='text'
          id='search'
          placeholder='Search'
          className='search'
          value={inputVal}
          onChange={handleChange}
        />
      </form>
    </SearchBarContentStyle>
  );
};

export default SearchBar;
