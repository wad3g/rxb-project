import styled from 'styled-components';

export const SearchBarContentStyle = styled.div`
  max-width: 1280px;
  width: 100%;
  height: 55px;
  background: rgba(212, 212, 212, 0.2);
  margin: 0 auto;
  border-radius: 40px;
  position: relative;
  color: #fff;

  input {
    font-size: 28px;
    position: absolute;
    border-bottom:1px solid #000;
    left: 0px;
    margin: 8px 0;
    padding: 0 0 0 60px;
    border: 0;
    width: 95%;
    background: transparent;
    height: 40px;
    color: #fff;
    box-sizing: border-box;

    :focus {
      outline: none;
    }
    ::placeholder {
      color: #fff;
      opacity: 1;
    }

    :-ms-input-placeholder {
      color: #fff;
    }

    ::-ms-input-placeholder {
      color: #fff;
    }

    @media screen and (max-width: 720px) {
      font-size: 28px;
    }
  }
`;
