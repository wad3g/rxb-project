import styled from 'styled-components';
import { color, fontFamily, fontSize, fontWeight, maxWidth, space } from 'styled-system';

const Subtitle = styled.h1`
  ${color}
  ${space}
  ${fontWeight}
  ${fontFamily}
  ${fontSize}
  ${maxWidth}

  & {
    line-height: 1;
    color: white;
    text-align: center;
    margin: 0;
    margin-bottom: 0.125rem;
  }
`;

Subtitle.defaultProps = {
  fontSize: fontSize,
  fontWeight: 600,
  fontFamily: 'var(--fontFamilySans)',
  mx: 'auto',
  maxWidth: '90%',
};

Subtitle.displayName = 'Subtitle';

export default Subtitle;
