import React from 'react';
import { ThumbnailStyled } from './ThumbnailStyled';
import { Link } from 'react-router-dom';
import PropTypes from 'prop-types';
import { Flex, Subtitle } from '../index';

const Thumbnail = ({
  image,
  movieId,
  movieName,
  clickable,
  title,
  voteCount,
}) => {
  return (
    <ThumbnailStyled bg={image} className='card'>
      {clickable ? (
        <>
          {/* Set styling for Link so the entire card is clickable */}
          <Link
            style={{ display: 'flex', alignItems: 'flex-end', height: '100%' }}
            to={`/movie/${movieId}`}>
            <Flex
              flexDirection='column'
              alignItems='center'
              justifyContent='center'
              width='100%'>
              {/* Set fontSize to be responsive-ish */}
              <Subtitle
                fontSize={[20, `calc(1rem + 0.8vw)`, `calc(1rem + 0.4vw)`]}>
                {title}
              </Subtitle>
              <span className='voteCount'>{voteCount}</span>
            </Flex>
          </Link>
        </>
      ) : (
        <>
          <img loading='lazy' src={image} alt='movieid' />
          <Subtitle fontSize={[20, `calc(1rem + 0.8vw)`, `calc(1rem + 0.4vw)`]}>
            >{title} {voteCount}
          </Subtitle>
        </>
      )}
    </ThumbnailStyled>
  );
};
Thumbnail.propTypes = {
  image: PropTypes.string,
  movieId: PropTypes.number,
  clickable: PropTypes.bool,
};
export default Thumbnail;
