import styled from 'styled-components';

export const ThumbnailStyled = styled.div`
  display: flex;
  flex-shrink: 0;
  justify-content: flex-end;
  flex-direction: column;
flex: 0 1 48%;
  height: 25vh;
  border-radius: 20px;
  margin-bottom: 1rem;
  background-image: linear-gradient(
      to bottom,
      rgba(23, 29, 54, 0.4),
      rgba(12, 12, 12, 0.5)
    ),
    ${(props) => `url(${props.bg})`};
  background-color: black;
  background-size: cover;
  background-repeat: no-repeat;
  background-position: center center;
  /* max-height: 350px; */
  transition: all 0.3s;
  object-fit: cover;
  border-radius: 10px;
  -webkit-box-shadow: 10px 10px 33px -4px rgba(0, 0, 0, 0.27);
  -moz-box-shadow: 10px 10px 33px -4px rgba(0, 0, 0, 0.27);
  box-shadow: 10px 10px 33px -4px rgba(0, 0, 0, 0.27);

  &:hover {
    background-image: linear-gradient(
        to bottom,
        rgba(23, 29, 54, 0.1),
        rgba(12, 12, 12, 0.1)
      ),
      ${(props) => `url(${props.bg})`};
  }

  @media screen and (min-width: 40rem) {
    flex: 0 1 32%;
    height: 20vh;
  }

  @media screen and (min-width: 60rem) {
    flex: 0 1 24%;
  }

  a {
    text-decoration: none;
  }
  .voteCount {
    font-size: 0.875rem;
    font-weight: 400;
    line-height: 2;
    color: white;
  }
  .voteCount::before {
    content: '★ ';
    color: #f7d839;
  }

  .clickable {
    cursor: pointer;
  }
`;
