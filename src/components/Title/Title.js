import styled from 'styled-components';
import {
  color,
  fontFamily,
  fontSize,
  fontWeight,
  space,
  lineHeight,
} from 'styled-system';

const Title = styled.h1`
    ${color}
    ${space}
    ${fontWeight}
    ${fontFamily}
    ${fontSize}
    ${lineHeight}

    & {
    letter-spacing: -0.0025rem;
  }
`;

Title.defaultProps = {
  fontSize: fontSize,
  fontWeight: 600,
  color: 'var(--headingColor)',
  mx: 'auto',
  lineHeight: 2,
};

Title.displayName = 'Title';

export default Title;
