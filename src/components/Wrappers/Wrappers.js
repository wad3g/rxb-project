import React, { Fragment } from 'react';
import { useParams } from 'react-router-dom';
import styled from 'styled-components';
import MovieDetail from '../../pages/MovieDetail';

export const AppWrapper = styled.div`
  display: flex;
  min-height: 100vh;
  flex-direction: column;
  width: 100%;
`;

export const PageWrapper = styled.div`
  font-size: large;
  padding: 2vh 6vw;
`;

export const ThumbnailWrapper = styled.div`
  display: flex;
  flex-wrap: wrap;
  width: 100%;
  justify-content: space-between;
  margin-top: 2rem;
`;

export const MovieWrapper = styled.div`
  margin-top: 20vh;
  display: flex;
  flex-flow: row;
`;

export const MovieInfoWrapper = styled.div`
  display: flex;
  flex-flow: column;
  min-width: 45vw;
`;

export function MovieDetailsWrapper() {
  const { id } = useParams();
  return (
    <Fragment>
      {id ? <MovieDetail  selectedMovie={id} /> : null}
    </Fragment>
  );
}
