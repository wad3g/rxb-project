// eslint-disable-next-line no-unused-vars
import { AppWrapper, PageWrapper, MovieDetailsWrapper, ThumbnailWrapper, MovieWrapper, MovieInfoWrapper } from "./Wrappers";

export { AppWrapper, PageWrapper, MovieDetailsWrapper, ThumbnailWrapper, MovieWrapper, MovieInfoWrapper } from './Wrappers';
