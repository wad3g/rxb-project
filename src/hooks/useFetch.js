import { useEffect, useState } from 'react';
import { BASE_URL } from '../utils/constants';

  export const fetchNextPage = async (url, pageNumber) => {
    try {
      const response = await fetch(BASE_URL + url + `&page=${pageNumber}`);
      const data = await response.json();
      return data;
    } catch (err) {}
  };

  export const useFetch = (url, params = {}, initialValue) => {
    const [data, setData] = useState(initialValue);
    const [isLoading, setIsLoading] = useState(false);
    const [hasError, setHasError] = useState(null);

    useEffect(
      () => {
        const getData = async () => {
          setIsLoading(true);
          try {
            const response = await fetch(
              BASE_URL + url,
              {
                headers: {
                  Accept: 'application/json',
                  'Content-Type': 'application/json;charset=utf-8',
                },
              }
            );
            const data = await response.json();
            setData(data);
            setIsLoading(false);
          } catch (error) {
            setHasError(error);
            setIsLoading(false);
          }
          setIsLoading(false);
        };

        getData();
      },
      [url],
      {}
    );
    return { data, isLoading, hasError };
  };
