import React, { Fragment } from "react";
import ReactDOM from "react-dom";
import { createGlobalStyle } from 'styled-components';
import "./index.css";
import App from "./App";
import reportWebVitals from "./reportWebVitals";


const GlobalStyle = createGlobalStyle`

  * {
    box-sizing: border-box;
    outline: none;
    text-rendering: optimizeSpeed;
    user-select: none;
    -webkit-font-smoothing: antialiased;
    -moz-osx-font-smoothing: grayscale;
    -webkit-text-size-adjust: none;
    -webkit-tap-highlight-color: rgba(0,0,0,0);
  }

  html {
    scroll-behavior: smooth;
    color: var(--font-color);

    -ms-text-size-adjust: 100%;
    -webkit-text-size-adjust: 100%
    -webkit-font-smoothing: antialiased;
    -moz-osx-font-smoothing: grayscale;
  }

  body {
    font-family: var(--fontFamily);
    line-height: 1.5;
    margin: 0;
    min-height: 100vh;
    -webkit-overflow-scrolling: touch;

  }

  input,
  textarea,
  select,
  button {
    color: inherit;
    font: inherit;
    letter-spacing: inherit;
  }

  input[type="text"],
  textarea {
    width: 100%;
  }

  input,
  textarea,
  button {
    border: 1px solid gray;
  }

  button {
    padding: 0.75em 1em;
    line-height: inherit;
    border-radius: 0;
    background-color: transparent;
  }

  button * {
    pointer-events: none;
  }

  a {
    cursor: pointer;
    font-weight: 500;
    transition: all 0.2s ease-in-out;
    text-decoration-color: currentColor;
    text-decoration-skip: ink;
  }
`;

ReactDOM.render(
  <Fragment>
    <App />
    <GlobalStyle />
  </Fragment>,
  document.getElementById("root")
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
