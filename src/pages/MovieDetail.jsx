import { Box, Flex, FlexAuto, Hero, Loader, MovieInfoWrapper, MovieWrapper, PageWrapper } from '../components';
import React, {useState} from 'react';
import { getTime, getYear } from '../utils/helpers';
import { useHistory, useParams } from 'react-router-dom';

import { API_KEY } from '../utils/constants';
import styled from 'styled-components'
import { tmdbConfig } from '../utils/config';
import { useFetch } from '../hooks';

var useConfig = tmdbConfig;

const MovieRating = styled(Box)`
  color: #E9BF3C;
  font-weight: 600;

  span {
    color: var(--fontColorTransparent) !important;
  }
`

const MovieInfoSpacer = styled.span`
  padding: 1rem;

  &::before {
    content: '|'
  }
`

const MovieOverview = styled.p`
  max-width: 50vw;
`

const BackButton = styled.button`
  background-color: transparent!important;
  font-family: var(--font);
  border: none;
  color: white;
  font-size: 0.875rem;
  position: absolute;
  line-height: 40px;
  vertical-align: top;
  font-size: 1.2rem;
  padding: 0;
`

const MovieDetail = () => {
  const { id } = useParams();
  const history = useHistory();
  const { data, isLoading, hasError } = useFetch(
    `/movie/${id}?api_key=${API_KEY}`
  );
  let movieDetails = [];

  const goBack = () => {
    history.goBack();
  };


  if (data && !isLoading) {
    movieDetails = data;
  }

  if (isLoading) {
    <Loader />
  }

  return (
    <PageWrapper>
        <BackButton
          onClick={goBack}
        >
          <span
            style={{
              fontSize: '1.2rem',
              lineHeight: '40px',
              verticalAlign: 'top',
            }}>
            &#8592;{' '}
          </span>
          <span style={{ lineHeight: '40px', verticalAlign: 'top' }}>Back</span>
        </BackButton>

        <Hero
          image={
            movieDetails.backdrop_path &&
            useConfig.imageUrl +
            useConfig.backdrops.hero +
            movieDetails.backdrop_path
          }
        />

        <MovieWrapper>

          <Box style={{ width: 'auto', marginTop: '10vh' }}>
            <Flex flexFlow='column'>
              <h1>{movieDetails.title}</h1>
            </Flex>

            <Flex flexFlow='row' alignItems='center' justifyContent='start'>
              <Box>{getYear(movieDetails.release_date)}</Box>
              <MovieInfoSpacer />
              <Box>{getTime(movieDetails.runtime)}</Box>
              <MovieInfoSpacer />
              <MovieRating>
                {movieDetails.vote_average}
                <span> ({movieDetails.vote_count})</span>
              </MovieRating>
            </Flex>
            <MovieInfoWrapper>
              <MovieOverview>{movieDetails.overview}</MovieOverview>
            </MovieInfoWrapper>
          </Box>

          <FlexAuto style={{ marginBottom: '10vh', paddingLeft: '5vw' }}>
            <img
              alt={movieDetails.title + 'Poster Image'}
              src={
                movieDetails.poster_path && useConfig.imageUrl +
                useConfig.posters.base +
                movieDetails.poster_path
              }
            />
          </FlexAuto>
        </MovieWrapper>

        {hasError && <span>There was an issue fetching your data.</span>}

    </PageWrapper>
  );
};

export default MovieDetail;
