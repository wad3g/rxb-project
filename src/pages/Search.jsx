import {
  Loader,
  PageWrapper,
  SearchBar,
  Thumbnail,
  ThumbnailWrapper,
} from '../components';
import React, { useContext, useEffect, useState } from 'react';

import { tmdbConfig } from '../utils/config';
import useLocalStorage from '../hooks/useLocalStorage';

var useConfig = tmdbConfig;
const getPrevResults = () => JSON.parse(localStorage.getItem('prevResults'));

function Search() {
  const prev = getPrevResults();
  const [prevResults, setPrevResults] = useLocalStorage('pervResults', prev);
  const [movies, setMovies] = useState(prevResults.results || []);
  const [isLoading, setIsLoading] = useState(false);
  const [hasError, setHasError] = useState(null);

  const searchMovieByTerm = (movieName) => {
    setIsLoading(true);
    fetch(
      `https://api.themoviedb.org/3/search/movie?&api_key=04c35731a5ee918f014970082a0088b1&query=` +
        movieName
    )
      .then((movies) => {
        return movies.json();
      })
      .then((moviesList) => {
        setMovies(moviesList.results);
        setPrevResults(moviesList);
        setIsLoading(false);
      })
      .catch((error) => {
        setHasError(error);
        setIsLoading(false);
      });
  };

  if (isLoading) {
    <Loader />;
  }

  return (
    <PageWrapper>
      <SearchBar searchMovieByTerm={searchMovieByTerm} />

      <ThumbnailWrapper>
        {movies &&
          movies.map((movie, idx) => (
            <Thumbnail
              key={idx}
              className='card'
              clickable
              image={
                movie.backdrop_path
                  ? useConfig.imageUrl +
                    useConfig.backdrops.hero +
                    movie.backdrop_path
                  : ''
              }
              movieId={movie.id}
              movieName={movie.original_title}
              voteCount={movie.vote_count}
              title={movie.title}
            />
          ))}
      </ThumbnailWrapper>

      {hasError && <span>There was an issue fetching your data.</span>}
    </PageWrapper>
  );
}

export default Search;
