import React from 'react';
import { Route } from 'react-router-dom';
import propTypes from 'prop-types';

export const AppRoute = ({ component: Component, layout: Layout, ...rest }) => (
  <Route
    {...rest}
    render={(props) => (
      <Layout>
        <Component {...props} />
      </Layout>
    )}
  />
);

AppRoute.propTypes = {
  ...propTypes.component,
  ...propTypes.layout,
};
