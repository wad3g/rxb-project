import { API_KEY, BASE_URL } from './constants';

const imageUrl = 'https://image.tmdb.org/t/p/';
const backdropSize = 'w1280';
const posterSize = 'w500';

const backdrops = ['w300', 'w780', 'w1280', 'original'];

const [card, featured, hero, background] = backdrops;

const posters = ['w92', 'w154', 'w185', 'w342', 'w500', 'w780', 'original'];

const [xs, sm, md, base, lg, xl, xxl] = posters;

export const tmdbConfig = {
  imageUrl,
  backdropSize,
  posterSize,
  backdrops: {
    card,
    featured,
    hero,
    background,
  },
  posters: {
    xs,
    sm,
    md,
    base,
    lg,
    xl,
    xxl,
  },
};

export const config = [
  {
    name: 'Popular',
    route: `/`,
    endpoint: `/movie/popular?api_key=${API_KEY}`,
  },
  {
    name: 'Now Playing',
    route: `/now`,
    endpoint: `/movie/now_playing?api_key=${API_KEY}`,
  },
  {
    name: 'Search',
    route: `/search`,
    endpoint: `${BASE_URL}/search/movie?api_key=${API_KEY}&query=`,
  },
  {
    name: 'Top Rated',
    route: `/top`,
    endpoint: `/movie/top_rated?api_key=${API_KEY}`,
  },
  {
    name: 'Upcoming',
    route: `/upcoming`,
    endpoint: `/movie/upcoming?api_key=${API_KEY}`,
  },
];
