var dayjs = require('dayjs')

// Convert runtime to display in hours and minutes
export const getTime = time => {
  const hours = Math.floor(time / 60);
  const mins = time % 60;
  return `${hours}h ${mins}m`;
};

// Get Year of release date for Movie Details page
export function getYear(date) {
    if (date === null || date === '') {
        return null;
    }
    return dayjs(date).year();
}

// mapOptions provides a list of options for a select field from a map of keys/values
// {"key": "value"}
// key is the value returned from the option, with value being the displayed option
export const mapOptions = m =>
  Object.keys(m).map(k => (
    <option key={k} value={k} >
      {m[k]}
    </option>
  ));
